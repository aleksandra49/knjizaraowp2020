package com.ftn.Knjizara.bean;

import java.util.HashMap;
import java.util.Locale;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.ftn.Knjizara.model.Korisnik;



@Configuration
public class SecondConfiguration implements WebMvcConfigurer{

	@Bean(name= {"memorijaAplikacije"}, 
			initMethod="init", destroyMethod="destroy")
	public MemorijaAplikacije getMemorijaAplikacije() {
		return new MemorijaAplikacije();
	}
	
	public class MemorijaAplikacije extends HashMap {
		
		@Override
		public String toString() {
			return "MemorijaAplikacije"+this.hashCode();
		}
		
		public void init() {
			
			System.out.println("init method called");
			Korisnik korisnik1 = new Korisnik("usernameKor", "passKor", "korisnik1@korisnik1",
					"ime","prezime","datumRodjenja","adresa",
					"brojTelefona","datum","uloga");
			this.put("korisnik1",korisnik1);
		}
		
		public void destroy() {
			
			System.out.println("destroy method called");
		}
	}
	
	@Bean(name= {"messageSource"})
    public ResourceBundleMessageSource messageSource() {

    	ResourceBundleMessageSource source = new ResourceBundleMessageSource();
        source.setBasenames("messages/messages");
        source.setUseCodeAsDefaultMessage(true);
        source.setDefaultEncoding("UTF-8");
        
//        source.setDefaultLocale(Locale.forLanguageTag("sr"));
        source.setDefaultLocale(Locale.ENGLISH);
        return source;
    }
	

	@Bean
	public LocaleResolver localeResolver() {
	    SessionLocaleResolver slr = new SessionLocaleResolver();
	    
	    slr.setDefaultLocale(Locale.forLanguageTag("sr"));
	    return slr;
	}
	
	@Bean
	public LocaleChangeInterceptor localeChangeInterceptor() {
	    LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
	    lci.setParamName("locale");
	    return lci;
	}
	
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
	    registry.addInterceptor(localeChangeInterceptor());
	}
}
