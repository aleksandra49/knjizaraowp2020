package com.ftn.Knjizara.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ftn.Knjizara.model.Korisnik;

@Controller
@RequestMapping(value="/Korisnici")
public class KorisnikController {
	
public static final String KORISNICI_KEY = "korisnici";
	
	@Autowired
	private ServletContext servletContext;
	private  String bURL; 
	
	
	@PostConstruct
	public void init() {	
		bURL = servletContext.getContextPath()+"/";		
		System.out.println(bURL + "ovo je burl");
		HashMap<String, Korisnik> korisnici = new HashMap<String, Korisnik>();
		korisnici.put("perac", new Korisnik("perac", "123", "pera@gmail.com", "Pera", "peraic","25.1.2020", "Jovanovica 3", "09485384582989", "3.3.2020.","admin" ));
		korisnici.put("zikic", new Korisnik("zikic", "123", "zika@gmail.com", "Zika", "zikaic","8.8.1988.", "Jaksica 3", "09485384582989", "3.3.2020.","korisnik"));
		korisnici.put("mikic", new Korisnik("mikic", "123", "miki@gmail,com", "Mika", "mikic","3.3.1999.", "Peraica 3", "09485384582989","3.3.2020.","korisnik" ));
		servletContext.setAttribute(KorisnikController.KORISNICI_KEY,korisnici);	
	}
	
	@GetMapping
	@ResponseBody
	public String index() {	
		HashMap<String, Korisnik> korisnici = (HashMap<String, Korisnik>)servletContext.getAttribute(KorisnikController.KORISNICI_KEY);
		
		StringBuilder retVal = new StringBuilder();
		retVal.append(
				"<!DOCTYPE html>\r\n" + 
				"<html>\r\n" + 
				"<head>\r\n" + 
				"	<meta charset=\"UTF-8\">\r\n" + 
	    		"	<base href=\""+bURL+"\">\r\n" + 
				"	<title>Korisnici</title>\r\n" + 
				"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviTabela.css\"/>\r\n" + 
				"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviHorizontalniMeni.css\"/>\r\n"+
				"</head>\r\n" + 
				"<body> "+
				"	<ul>\r\n" + 
				"		<li><a href=\"Knjige\">Knjige</a></li>\r\n" + 
				"		<li><a href=\"Knjiga\">Knjiga</a></li>\r\n" + 
				"		<li><a href=\"Korisnici\">Korisnici</a></li>\r\n" +
				"	</ul>\r\n" + 
				"		<table>\r\n" + 
				"			<caption>Korisnici</caption>\r\n" + 
				"			<tr>\r\n" + 
				"				<th>Broj</th>\r\n" +
				"				<th>Ime</th>\r\n" + 
				"				<th>Prezime</th>\r\n" + 
				"				<th>Korisničko ime</th>\r\n" + 
				
				"			</tr>\r\n");
		
		List<Korisnik> users = new ArrayList<Korisnik>(korisnici.values());
		for (int i=0; i < users.size(); i++) {
			retVal.append(
				"			<tr>\r\n" + 
				"				<td class=\"broj\">"+ (i+1) +"</td>\r\n" +
				"				<td>"+ users.get(i).getIme() +"</td>\r\n" +		
				"				<td>"+ users.get(i).getPrezime() +"</td>\r\n" +
				"				<td><a href=\"Korisnici/Details?korisnickoIme="+
									users.get(i).getKorIme()+"\">" +users.get(i).getKorIme() +"</a></td>\r\n" +
				"			</tr>\r\n");
		}
		retVal.append(
				"		</table>\r\n");
		retVal.append(
				"	<ul>\r\n" + 
				"		<li><a href=\"Korisnici/Create\">Registracija korisnika</a></li>\r\n" + 
				"	</ul>\r\n");
		retVal.append(
				"</body>\r\n"+
				"</html>\r\n");		
		return retVal.toString();
	}
	
	@GetMapping(value="/Details")
	@ResponseBody
	public String details(@RequestParam String korisnickoIme) {	
		HashMap<String, Korisnik> korisnici = (HashMap<String, Korisnik>)servletContext.getAttribute(KorisnikController.KORISNICI_KEY);
		Korisnik korisnik = korisnici.get(korisnickoIme);
		if(korisnik==null) {
			korisnik = new Korisnik();
			StringBuilder retVal = new StringBuilder();
			retVal.append(vratiHTMLZaKorisnika("Create", null, korisnik, korisnik.getLozinka(), "Korisnik", "Korisnik", "Korisnici/Create"));	
			return retVal.toString();
		}
		
		StringBuilder retVal = new StringBuilder();
		retVal.append(vratiHTMLZaKorisnika("Details", null, korisnik, korisnik.getLozinka(), "Korisnik", "Korisnik", "Korisnici/Edit"));	
		return retVal.toString();
	}
	
	@GetMapping(value="/Create")
	@ResponseBody
	public String create() {	
		Korisnik korisnik = new Korisnik();
		StringBuilder retVal = new StringBuilder();
		retVal.append(vratiHTMLZaKorisnika("Create", null, korisnik, korisnik.getLozinka(), "Korisnik", "Korisnik", "Korisnici/Create"));	
		return retVal.toString();
	}
	
	@SuppressWarnings("unchecked")
	@PostMapping(value="/Create")
	public void create(@ModelAttribute Korisnik korisnik, @RequestParam(defaultValue="") String ponovljenaSifra, 
			BindingResult result, HttpServletResponse response) throws IOException {
		
		HashMap<String, Korisnik> korisnici = (HashMap<String, Korisnik>)
				servletContext.getAttribute(KorisnikController.KORISNICI_KEY);
		String greska = "";
		if (result.hasErrors()) {
			greska = "greška prilikom preuzimanja podataka<br/>";
        }
		if(korisnik.getKorIme().trim().equals(""))
			greska+="korisničko ime nije uneseno<br/>";
		if(korisnici.get(korisnik.getKorIme())!=null)
			greska+="korisničko ime nije jedinstveno<br/>";
		if(korisnik.getLozinka().trim().equals(""))
			greska+="korisnička šifra nije unesena<br/>";
		if(!korisnik.getLozinka().equals(ponovljenaSifra))
			greska+="korisničke šifre se ne podudaraju<br/>";
		
		if(!greska.equals("")) {
			response.setContentType("text/html; charset=UTF-8");
			PrintWriter out;	
			out = response.getWriter();
			out.write(vratiHTMLZaKorisnika("Create", greska, korisnik, ponovljenaSifra, "Korisnik", "Korisnik", "Korisnici/Create"));
			return;
		}
		
		korisnici.put(korisnik.getKorIme(), korisnik);
		response.sendRedirect(bURL+"Korisnici");
	}
	 

	@SuppressWarnings("unchecked")
	@PostMapping(value="/Edit")
	public void edit(@ModelAttribute Korisnik korisnikEdited, @RequestParam String ponovljenaSifra, 
			BindingResult result, HttpServletResponse response) throws IOException {	
		
		HashMap<String, Korisnik> korisnici = (HashMap<String, Korisnik>)
				servletContext.getAttribute(KorisnikController.KORISNICI_KEY);
		String greska = "";
		if (result.hasErrors() || korisnikEdited==null) {
			greska = "greška prilikom preuzimanja podataka<br/>";
        }
		if(korisnici.get(korisnikEdited.getKorIme())==null)
			greska+="korisnik ne postoji u evidenciji<br/>";
		if(!korisnikEdited.getLozinka().equals(ponovljenaSifra))
			greska+="korisničke šifre se ne podudaraju<br/>";
		
		if(!greska.equals("")) {
			response.setContentType("text/html; charset=UTF-8");
			PrintWriter out;	
			out = response.getWriter();
			out.write(vratiHTMLZaKorisnika("Details", greska, korisnikEdited, ponovljenaSifra, "Korisnik", "Korisnik", "Korisnici/Edit"));
			return;
		}
		
		Korisnik korisnik = korisnici.get(korisnikEdited.getKorIme());
		if(korisnik!=null && korisnik.getLozinka().equals(ponovljenaSifra)) {
			if(korisnikEdited.getEmail()!=null && !korisnikEdited.getEmail().trim().equals(""))
				korisnik.setEmail(korisnikEdited.getEmail());
			if(korisnikEdited.getIme()!=null && !korisnikEdited.getIme().trim().equals(""))
				korisnik.setIme(korisnikEdited.getIme());
			if(korisnikEdited.getPrezime()!=null && !korisnikEdited.getPrezime().trim().equals(""))
				korisnik.setPrezime(korisnikEdited.getPrezime());
			
			if(korisnikEdited.getDatumRodjenja()!=null && !korisnikEdited.getDatumRodjenja().trim().equals(""))
				korisnik.setDatumRodjenja(korisnikEdited.getDatumRodjenja());
			if(korisnikEdited.getAdresa()!=null && !korisnikEdited.getAdresa().trim().equals(""))
				korisnik.setAdresa(korisnikEdited.getAdresa());
			
			if(korisnikEdited.getBrojTel()!=null && !korisnikEdited.getBrojTel().trim().equals(""))
				korisnik.setBrojTel(korisnikEdited.getBrojTel());
			if(korisnikEdited.getDatIvremeReg()!=null && !korisnikEdited.getDatIvremeReg().trim().equals(""))
				korisnik.setDatIvremeReg(korisnikEdited.getDatIvremeReg());
			
			if(korisnikEdited.getUloga()!=null && !korisnikEdited.getUloga().trim().equals(""))
				korisnik.setUloga(korisnikEdited.getUloga());
			
			
			if(korisnikEdited.getLozinka()!=null && !korisnikEdited.getLozinka().trim().equals(""))
				korisnik.setLozinka(korisnikEdited.getLozinka());
		}
		response.sendRedirect(bURL+"Korisnici");
	}
	
	
	private String vratiHTMLZaKorisnika(String rezimPrikaza, String greska, Korisnik korisnik, String ponovljenaSifra, 
			String title, String caption, String formURL) {
		String dugme = "";
		if(rezimPrikaza.equals("Create"))
			dugme="Dodaj";
		else if (rezimPrikaza.equals("Details"))
			dugme="Izmeni";
		
		StringBuilder retVal = new StringBuilder();
		retVal.append(
				"<!DOCTYPE html>\r\n" + 
				"<html>\r\n" + 
				"<head>\r\n" + 
				"	<meta charset=\"UTF-8\">\r\n" + 
				"	<base href=\""+bURL+"\">\r\n" +
				"	<title>"+title+"</title>\r\n" + 
				"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviForma.css\"/>\r\n" + 
				"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviHorizontalniMeni.css\"/>\r\n"+
				"</head>\r\n" + 
				"<body>\r\n" + 
				"	<ul>\r\n" + 
				"		<li><a href=\"Knjige\">Knjige</a></li>\r\n" + 
				"		<li><a href=\"Knjiga\">Knjiga</a></li>\r\n" + 
				"		<li><a href=\"Korisnici\">Korisnici</a></li>\r\n" +
				"	</ul>\r\n");
		if(greska!=null)
			retVal.append(
				"	<div>"+greska+"</div>\r\n");		
		retVal.append(		
				"	<form method=\"post\" action=\""+formURL+"\">\r\n" + 
				"		<table>\r\n" + 
				"			<caption>"+caption+"</caption>\r\n" + 
				"			<tr><th>Ime:</th><td><input type=\"text\" "+
 								"value=\""+korisnik.getIme()+"\" name=\"ime\"/></td></tr>\r\n" +
 				"			<tr><th>Prezime:</th><td><input type=\"text\" "+
 								"value=\""+korisnik.getPrezime()+"\" name=\"prezime\"/></td></tr>\r\n" +

 				"			<tr><th>Email:</th><td><input type=\"text\" "+
								"value=\""+korisnik.getEmail()+"\" name=\"email\"/></td></tr>\r\n" + 
				"			<tr><th>Datum rodjenja:</th><td><input type=\"text\" "+
 								"value=\""+korisnik.getDatumRodjenja()+"\" name=\"datumRodjenja\"/></td></tr>\r\n" + 	
 				"			<tr><th>Adresa:</th><td><input type=\"text\" "+
 								"value=\""+korisnik.getAdresa()+"\" name=\"adresa\"/></td></tr>\r\n" +				 				
 				"			<tr><th>Broj telefona:</th><td><input type=\"text\" "+
 								"value=\""+korisnik.getBrojTel()+"\" name=\"brojTel\"/></td></tr>\r\n" + 								
 				"			<tr><th>Datum i vreme reg:</th><td><input type=\"text\" "+
 								"value=\""+korisnik.getDatIvremeReg()+"\" name=\"datIvremeReg\"/></td></tr>\r\n" + 				
 				"			<tr><th>Uloga:</th><td><input type=\"text\" "+
 								"value=\""+korisnik.getUloga()+"\" name=\"uloga\"/></td></tr>\r\n" + 				
 				
 				
				"			<tr><th>Korisničko ime:</th><td><input type=\"text\" "+
 								"value=\""+korisnik.getKorIme()+"\" name=\"korIme\" "+(rezimPrikaza.equals("Details")?"readonly":"")+ "/></td></tr>\r\n" +
 				"			<tr><th>Lozinka:</th><td><input type=\"password\" "+
 								"value=\""+korisnik.getLozinka()+"\" name=\"lozinka\"/></td></tr>\r\n" +
 				"			<tr><th>ponovljena šifra:</th><td><input type=\"password\" "+
 								"value=\""+ponovljenaSifra+"\" name=\"ponovljenaSifra\"/></td></tr>\r\n" +  
 				"			<tr><th></th><td><input type=\"submit\" value=\""+dugme+"\" /></td>\r\n" + 
				"		</table>\r\n" + 
				"	</form>\r\n" + 
				"	<br/>\r\n");
		
		retVal.append(
				"</body>\r\n"+
				"</html>\r\n");		
		return retVal.toString();
		
	}

}
