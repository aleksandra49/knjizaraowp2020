package com.ftn.Knjizara.controller;

import java.io.IOException;
import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ftn.Knjizara.model.Korisnik;



@Controller
@RequestMapping("PrijavaOdjavaController")
public class PrijavaOdjavaController {
	
	public static final String ULOGOVAN_KEY = "ulogovanKorisnik";
	
	@Autowired
	private ServletContext servletContext;
	private  String bURL; 

	@PostConstruct
	public void init() {	
		bURL = servletContext.getContextPath()+"/";				
	}
	
	
	@PostMapping("/LogIn")
	public void logovanje(@RequestParam String korIme, @RequestParam String lozinka,
			HttpSession session, HttpServletResponse response) throws IOException {

		HashMap<String, Korisnik> korisnici = (HashMap<String, Korisnik>)servletContext.getAttribute(KorisnikController.KORISNICI_KEY);		
		if(korIme==null || korIme.trim().equals("")) {
			response.sendRedirect(bURL+"login.html");
			return;
		}
		if(lozinka==null || lozinka.trim().equals("")) {
			response.sendRedirect(bURL+"login.html");
			return;
		}
		if(korisnici.get(korIme)==null) {
			response.sendRedirect(bURL+"login.html");
			return;
		}
		if(!korisnici.get(korIme).getLozinka().equals(lozinka)) {
			response.sendRedirect(bURL+"login.html");
			return;
		}
		if(korisnici.get(korIme).isUlogovan()) {
			response.sendRedirect(bURL+"login.html");
			return;
		}
		if(session.getAttribute(ULOGOVAN_KEY)!=null) {
			response.sendRedirect(bURL+"login.html");
			return;
		}
		korisnici.get(korIme).setUlogovan(true);
		session.setAttribute(ULOGOVAN_KEY, korisnici.get(korIme));
		response.sendRedirect(bURL+"Knjige");
	}
}
