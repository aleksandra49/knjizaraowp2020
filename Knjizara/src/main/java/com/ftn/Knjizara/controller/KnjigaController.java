package com.ftn.Knjizara.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;


import com.ftn.Knjizara.model.Knjiga;
import com.ftn.Knjizara.model.Knjige;
import com.ftn.Knjizara.model.Korisnik;
import com.ftn.Knjizara.model.Zanr;
import com.ftn.Knjizara.service.KnjigaService;
import com.ftn.Knjizara.service.KupovinaService;
import com.ftn.Knjizara.service.ZanrService;



@Controller
@RequestMapping(value="/Knjiga")
public class KnjigaController implements ServletContextAware  /* ApplicationContextAware*/ {
	
public static final String Knjiga_KEY = "knjiga";
	

private ServletContext servletContext;
private  String bURL; 

	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
} 

@PostConstruct
public void init() {
	
	bURL = servletContext.getContextPath()+"/";
	
}

@GetMapping
@ResponseBody
public String index(HttpSession session) {	

	Knjige knjige = (Knjige)servletContext.getAttribute(KnjigeController.KNJIGE_KEY);
	
	@SuppressWarnings("unchecked")
	
	StringBuilder retVal = new StringBuilder();
	retVal.append(
			"<!DOCTYPE html>\r\n" + 
			"<html>\r\n" + 
			"<head>\r\n" + 
			"	<meta charset=\"UTF-8\">\r\n" + 
    		"	<base href=\""+bURL+"\">\r\n" + 
			"	<title>Knjiga</title>\r\n" + 
			"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviTabela.css\"/>\r\n" + 
			"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviHorizontalniMeni.css\"/>\r\n"+
			"</head>\r\n" + 
			"<body> "+
			"	<ul>\r\n" + 
			"		<li><a href=\"Knjige\">Knjige</a></li>\r\n" + 
			"		<li><a href=\"Knjiga\">Knjiga</a></li>\r\n" +
			"		<li><a href=\"Korisnici\">Korisnici</a></li>\r\n" +
			"	</ul>\r\n" + 
			"		<table>\r\n" + 
			"			<caption>Knjige</caption>\r\n" + 
			"			<tr>\r\n" + 
			"				<th>broj</th>\r\n" + 
			"				<th>naziv</th>\r\n" + 
			"				<th>ISBN</th>\r\n" + 
			"				<th>izdavacka kuca</th>\r\n" +
			"				<th>autor</th>\r\n" +
			"				<th>godina izdavanja</th>\r\n" + 
			"				<th>opis</th>\r\n" + 
			"				<th>cena</th>\r\n" + 
			"				<th>brojstr</th>\r\n" +
			"				<th>tippoveza</th>\r\n" +				
			"				<th>pismo</th>\r\n" +
			"				<th>jezik</th>\r\n" +				
			"			</tr>\r\n");
	
	List<Knjiga> ud = knjige.findAll();
	for (int i=0; i < ud.size(); i++) {
		retVal.append(
			"			<tr>\r\n" + 
			"				<td class=\"broj\">"+ (i+1) +"</td>\r\n" + 
			"				<td><a href=\"Knjige/Details?id="+ud.get(i).getId()+"\">" +ud.get(i).getNaziv() +"</a></td>\r\n" +
			"				<td><a href=\"Knjige/Details?id="+ud.get(i).getId()+"\">" +ud.get(i).getISBN() +"</a></td>\r\n" +
			"				<td><a href=\"Knjige/Details?id="+ud.get(i).getId()+"\">" +ud.get(i).getIzdavackaKuca() +"</a></td>\r\n" +
			"				<td><a href=\"Knjige/Details?id="+ud.get(i).getId()+"\">" +ud.get(i).getAutor() +"</a></td>\r\n" +
			"				<td><a href=\"Knjige/Details?id="+ud.get(i).getId()+"\">" +ud.get(i).getGodIzdavanja() +"</a></td>\r\n" +
			"				<td><a href=\"Knjige/Details?id="+ud.get(i).getId()+"\">" +ud.get(i).getOpis() +"</a></td>\r\n" +
			//"				<td><a href=\"Knjige/Details?id="+ud.get(i).getId()+"\">" +ud.get(i).getSlika() +"</a></td>\r\n" +
			"				<td><a href=\"Knjige/Details?id="+ud.get(i).getId()+"\">" +ud.get(i).getCena() +"</a></td>\r\n" +	
			"				<td><a href=\"Knjige/Details?id="+ud.get(i).getId()+"\">" +ud.get(i).getBrojstr() +"</a></td>\r\n" +
			"				<td><a href=\"Knjige/Details?id="+ud.get(i).getId()+"\">" +ud.get(i).getTip_poveza() +"</a></td>\r\n" +
			"				<td><a href=\"Knjige/Details?id="+ud.get(i).getId()+"\">" +ud.get(i).getPismo() +"</a></td>\r\n" +
			"				<td><a href=\"Knjige/Details?id="+ud.get(i).getId()+"\">" +ud.get(i).getJezik() +"</a></td>\r\n" +				
		//	"				<td>\r\n"+				
		//	"					<a href=\"Knjiga?KnjigaID="+ud.get(i).getId()+"\">knjiga</a>\r\n" + 
		//	"					<form action=\"Knjiga\" method=\"get\">\r\n" + 
		//	"						<input type=\"hidden\" name=\"KnjigaID\" value=\""+ud.get(i).getId()+"\"/>\r\n" + 
		//	"						<input type=\"submit\" value=\"Knjiga\"/>\r\n" + 
			"					</form>\r\n" + 
			"				</td>\r\n"+
			"			</tr>\r\n");
	}
	retVal.append(
			"		</table>\r\n");
	retVal.append(
			"	<ul>\r\n" + 
			"		<li><a href=\"Knjige/Create\">Dodavanje Knjiga</a></li>\r\n" + 
			"	</ul>\r\n");
	return retVal.toString();
}
	

@GetMapping(value="/Details")
@ResponseBody
public String details(@RequestParam Long id, HttpServletRequest request) {	

	Knjige knjige = (Knjige)servletContext.getAttribute(KnjigeController.KNJIGE_KEY);
	Knjiga knjiga = knjige.findAll().get(id.intValue()-1);
	
	StringBuilder retVal = new StringBuilder();
	retVal.append(
			"<!DOCTYPE html>\r\n" + 
			"<html>\r\n" + 
			"<head>\r\n" + 
			"	<meta charset=\"UTF-8\">\r\n" + 
			"	<base href=\""+bURL+"\">\r\n" +
			"	<title>Knjiga</title>\r\n" + 
			"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviForma.css\"/>\r\n" + 
			"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviHorizontalniMeni.css\"/>\r\n"+
			"</head>\r\n" + 
			"<body>\r\n" + 
			"	<ul>\r\n" + 
			"		<li><a href=\"Knjige\">Knjige</a></li>\r\n" + 
			"		<li><a href=\"Knjiga\">Knjiga</a></li>\r\n" + 
			"	</ul>\r\n" + 				
			"	<form method=\"post\" action=\"Knjige/Edit\">\r\n" + 
			"		<input type=\"hidden\" name=\"id\" value=\""+knjiga.getId()+"\">\r\n" + 
			"		<table>\r\n" + 
			"			<caption>Knjiga</caption>\r\n" + 
			"			<tr><th>naziv:</th><td><input type=\"text\" "+
			 				"value=\""+knjiga.getNaziv()+"\" name=\"naziv\"/></td></tr>\r\n" + 
			 				"value=\""+knjiga.getISBN()+"\" name=\"ISBN\"/></td></tr>\r\n" + 
			 				"value=\""+knjiga.getIzdavackaKuca()+"\" name=\"izdavckaKuca\"/></td></tr>\r\n" + 
			 				"value=\""+knjiga.getAutor()+"\" name=\"autori\"/></td></tr>\r\n" + 
			 				"value=\""+knjiga.getGodIzdavanja()+"\" name=\"godinaIzdavanja\"/></td></tr>\r\n" + 
			 				"value=\""+knjiga.getOpis()+"\" name=\"opis\"/></td></tr>\r\n" + 
			 				//"value=\""+knjiga.getSlika()+"\" name=\"slika\"/></td></tr>\r\n" + 
			 				"value=\""+knjiga.getCena()+"\" name=\"cena\"/></td></tr>\r\n" + 
			 				"value=\""+knjiga.getBrojstr()+"\" name=\"brstr\"/></td></tr>\r\n" + 
			 				"value=\""+knjiga.getTip_poveza()+"\" name=\"tip\"/></td></tr>\r\n" + 
			 				"value=\""+knjiga.getPismo()+"\" name=\"pismo\"/></td></tr>\r\n" + 
			 				"value=\""+knjiga.getJezik()+"\" name=\"jezik\"/></td></tr>\r\n" + 
			"			<tr><th></th><td><input type=\"submit\" value=\"Izmeni\" /></td>\r\n" + 
			"		</table>\r\n" + 
			"	</form>\r\n" + 
			"	<br/>\r\n" + 
			"	<form method=\"post\" action=\"Knjige/Delete\">\r\n" + 
			"		<input type=\"hidden\" name=\"id\" value=\""+knjiga.getId()+"\">\r\n" + 
			"		<table>\r\n" + 
			"			<tr><th></th><td><input type=\"submit\" value=\"Obriši\"></td>\r\n" + 
			"		</table>\r\n" + 
			"	</form>\r\n");
	retVal.append(
			"</body>\r\n"+
			"</html>\r\n");		
	return retVal.toString();
}
	

@GetMapping(value="/Create")
@ResponseBody
public String create(HttpSession session) {
	
	StringBuilder retVal = new StringBuilder();
	retVal.append(
			"<!DOCTYPE html>\r\n" + 
			"<html>\r\n" + 
			"<head>\r\n" + 
			"	<meta charset=\"UTF-8\">\r\n" + 
			"	<base href=\""+bURL+"\">\r\n" + 
			"	<title>Knjiga kreiranje</title>\r\n" + 
			"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviForma.css\"/>\r\n" + 
			"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviHorizontalniMeni.css\"/>\r\n"+
			"</head>\r\n" + 
			"<body>\r\n" + 
			"	<ul>\r\n" + 
			"		<li><a href=\"Knjige\">Knjige</a></li>\r\n" + 
			"		<li><a href=\"Knjiga\">Knjiga</a></li>\r\n" + 
			"	</ul>\r\n" + 
			"	<form method=\"post\" action=\"Knjige/Create\">\r\n" + 
			"		<table>\r\n" + 
			"			<caption>Knjiga</caption>\r\n" + 
			"			<tr><th>naziv:</th><td><input type=\"text\" value=\"\" name=\"naziv\"/></td></tr>\r\n" + 
			"			<tr><th>ISBN:</th><td><input type=\"text\" value=\"\" name=\"ISBN\"/></td></tr>\r\n" +
			"			<tr><th>IzdavackaKuca:</th><td><input type=\"text\" value=\"\" name=\"izdavackaKuca\"/></td></tr>\r\n" + 
			"			<tr><th>Autroi:</th><td><input type=\"text\" value=\"\" name=\"autori\"/></td></tr>\r\n" + 
			"			<tr><th>GodinaIzdavanja:</th><td><input type=\"text\" value=\"\" name=\"godinaIzdavanja\"/></td></tr>\r\n" +
			"			<tr><th>Opis:</th><td><input type=\"text\" value=\"\" name=\"opis\"/></td></tr>\r\n" + 
			//"			<tr><th>Slika:</th><td><input type=\"text\" value=\"\" name=\"slika\"/></td></tr>\r\n" + 
			"			<tr><th>Cena:</th><td><input type=\"text\" value=\"\" name=\"cena\"/></td></tr>\r\n" +
			"			<tr><th>BrojStrana:</th><td><input type=\"text\" value=\"\" name=\"brstr\"/></td></tr>\r\n" + 				
			"			<tr><th>tip:</th><td><input type=\"text\" value=\"\" name=\"tip\"/></td></tr>\r\n" + 
			"			<tr><th>Pismo:</th><td><input type=\"text\" value=\"\" name=\"pismo\"/></td></tr>\r\n" + 
			"			<tr><th>Jezik:</th><td><input type=\"text\" value=\"\" name=\"jezik\"/></td></tr>\r\n" +
			"			<tr><th></th><td><input type=\"submit\" value=\"Dodaj\" /></td>\r\n" + 
			"		</table>\r\n" + 
			"	</form>\r\n" +
			"	<br/>\r\n");
	retVal.append(
			"</body>\r\n"+
			"</html>\r\n");
	return retVal.toString();	
}
	

}
