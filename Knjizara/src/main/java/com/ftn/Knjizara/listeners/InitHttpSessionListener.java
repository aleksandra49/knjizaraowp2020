package com.ftn.Knjizara.listeners;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.springframework.stereotype.Component;

import com.ftn.Knjizara.controller.KnjigaController;
import com.ftn.Knjizara.model.Knjiga;



@Component
public class InitHttpSessionListener implements HttpSessionListener {

	/** kod koji se izvrsava po kreiranju sesije */
	public void sessionCreated(HttpSessionEvent event) {
		System.out.println("Inicijalizacija sesisje HttpSessionListener...");

		List<Knjiga> pogledaneKNjige = new ArrayList<Knjiga>();

		HttpSession session  = event.getSession();
		System.out.println("Session id korisnika je "+ session.getId());
		//session.setAttribute(InternacionalizacijaController.LOKALIYACIJA_KEY, Locale.forLanguageTag("sr"));

		

		System.out.println("Uspeh HttpSessionListener!");
	}
	
	/** kod koji se izvrsava po brisanju sesije */
	public void sessionDestroyed(HttpSessionEvent arg0) {
		System.out.println("Brisanje sesisje HttpSessionListener...");
		
		System.out.println("Uspeh HttpSessionListener!");
	}

}
