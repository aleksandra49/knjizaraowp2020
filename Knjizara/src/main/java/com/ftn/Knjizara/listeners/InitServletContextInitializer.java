package com.ftn.Knjizara.listeners;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.stereotype.Component;


import com.ftn.Knjizara.controller.KnjigaController;
import com.ftn.Knjizara.controller.KnjigeController;
import com.ftn.Knjizara.model.Knjige;

@Component
public final class InitServletContextInitializer implements ServletContextInitializer {
 
	
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
    	System.out.println("Inicijalizacija konteksta pri ServletContextInitializer...");

		servletContext.setAttribute(KnjigeController.KNJIGE_KEY, new Knjige());
		
    	System.out.println("Uspeh ServletContextInitializer!");
    }

}