package com.ftn.Knjizara.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.ftn.Knjizara.model.Korisnik;
import com.ftn.Knjizara.service.KorisnikService;



@Service
public class InMemoryKorisnikService implements KorisnikService {

	/*HashMap<String, Korisnik> korisnici = new HashMap<>();

	@PostConstruct
    private void iniDataForTesting() {	
		save(new Korisnik());
		save(new Korisnik());
		save(new Korisnik());
    }

	@Override
	public Korisnik findOne(String korisnickoIme) {
		return korisnici.get(korisnickoIme);
	}

	@Override
	public Korisnik findOne(String korisnickoIme, String lozinka) {
		Korisnik korisnik = findOne(korisnickoIme);
		if (korisnik != null && korisnik.getLozinka().equals(lozinka))
			return korisnik;
		return null;
	}

	@Override
	public List<Korisnik> findAll() {
		return new ArrayList<Korisnik>(korisnici.values());
	}

	@Override
	public Korisnik save(Korisnik korisnik) {
	
		if (korisnici.get(korisnik.getKorisnickoIme()) == null) {
			korisnici.put(korisnik.getKorisnickoIme(), korisnik);
		}
		
		return korisnik;
	}

	@Override
	public List<Korisnik> save(List<Korisnik> korisnici) {
		List<Korisnik> rezultat = new ArrayList<>();
		for (Korisnik itKorisnik: korisnici) {
			
			Korisnik saved = save(itKorisnik);

			if (saved != null) {
				rezultat.add(saved);
			}
		}

		return rezultat;
	}

	@Override
	public Korisnik update(Korisnik korisnik) {	
		
		if (korisnici.get(korisnik.getKorisnickoIme()) != null) {
			korisnici.put(korisnik.getKorisnickoIme(), korisnik);
		}
		return korisnik;
	}

	@Override
	public List<Korisnik> update(List<Korisnik> korisnici) {
		List<Korisnik> rezultat = new ArrayList<>();
		for (Korisnik itKorisnik: korisnici) {
			
			Korisnik updated = update(itKorisnik);

			if (updated != null) {
				rezultat.add(updated);
			}
		}

		return rezultat;
	}

	@Override
	public Korisnik delete(String korisnickoIme) {
		if (!korisnici.containsKey(korisnickoIme)) {
			throw new IllegalArgumentException("invalid id");
		}

		Korisnik korisnik = korisnici.get(korisnickoIme);
		if (korisnik != null) {
			korisnici.remove(korisnickoIme);
		}
		return korisnik;
	}

	@Override
	public void delete(List<String> korisnickaImena) {
		for (String itKorisnickoIme: korisnickaImena) {
			delete(itKorisnickoIme);
		}
	}

	@Override
	public List<Korisnik> find(String korisnickoIme, String eMail, Boolean administrator) {
		if (korisnickoIme == null) {
			korisnickoIme = "";
		}
		if (eMail == null) {
			eMail = "";
		}
		if (administrator == null) {
			administrator = false;
		}

		List<Korisnik> rezultat = new ArrayList<>();
		for (Korisnik itKorisnik: korisnici.values()) {
			if (!itKorisnik.getKorisnickoIme().toLowerCase().contains(korisnickoIme.toLowerCase())) {
				continue;
			}
			if (!itKorisnik.getEMail().toLowerCase().contains(eMail.toLowerCase())) {
				continue;
			}
			
			if (!(administrator && itKorisnik.isAdministrator())) {
				continue;
			}

			rezultat.add(itKorisnik);
		}

		return rezultat;
	}

	@Override
	public List<Korisnik> findByKorisnickoIme(String korisnickoIme) {
		if (korisnickoIme == null) {
			korisnickoIme = "";
		}

		List<Korisnik> rezultat = new ArrayList<>();
		for (Korisnik itKorisnik: korisnici.values()) {
			if (!itKorisnik.getKorisnickoIme().toLowerCase().contains(korisnickoIme.toLowerCase())) {
				continue;
			}

			rezultat.add(itKorisnik);
		}

		return rezultat;
	}

*/
}
