package com.ftn.Knjizara.model;

import java.util.ArrayList;

public class Kupovina {
	
	private ArrayList<Knjiga> knjige = new ArrayList<Knjiga>();
	//private Knjiga knjige;
	private int ukupnaCena;
	private String datumKopuvine;
	//private LocalDateTime datumKopuvine;
	private Korisnik korisnikKupio;
	private int brojKupKnjiga;
	
	
	
	public Kupovina() {
		super();
	}



	public Kupovina(ArrayList<Knjiga> knjige, int ukupnaCena, String datumKopuvine, Korisnik korisnikKupio,
			int brojKupKnjiga) {
		super();
		this.knjige = knjige;
		this.ukupnaCena = ukupnaCena;
		this.datumKopuvine = datumKopuvine;
		this.korisnikKupio = korisnikKupio;
		this.brojKupKnjiga = brojKupKnjiga;
	}



	public ArrayList<Knjiga> getKnjige() {
		return knjige;
	}



	public void setKnjige(ArrayList<Knjiga> knjige) {
		this.knjige = knjige;
	}



	public int getUkupnaCena() {
		return ukupnaCena;
	}



	public void setUkupnaCena(int ukupnaCena) {
		this.ukupnaCena = ukupnaCena;
	}



	public String getDatumKopuvine() {
		return datumKopuvine;
	}



	public void setDatumKopuvine(String datumKopuvine) {
		this.datumKopuvine = datumKopuvine;
	}



	public Korisnik getKorisnikKupio() {
		return korisnikKupio;
	}



	public void setKorisnikKupio(Korisnik korisnikKupio) {
		this.korisnikKupio = korisnikKupio;
	}



	public int getBrojKupKnjiga() {
		return brojKupKnjiga;
	}



	public void setBrojKupKnjiga(int brojKupKnjiga) {
		this.brojKupKnjiga = brojKupKnjiga;
	}



	@Override
	public String toString() {
		return "Kupovina [knjige=" + knjige + ", ukupnaCena=" + ukupnaCena + ", datumKopuvine=" + datumKopuvine
				+ ", korisnikKupio=" + korisnikKupio + ", brojKupKnjiga=" + brojKupKnjiga + "]";
	}
	
	

}
