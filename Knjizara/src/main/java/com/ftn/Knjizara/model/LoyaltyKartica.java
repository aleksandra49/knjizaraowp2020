package com.ftn.Knjizara.model;

public class LoyaltyKartica {
	
	private String popust;
	private int broj_bodova;
	
	
	
	public LoyaltyKartica() {
		
	}

	public LoyaltyKartica(String popust, int broj_bodova) {
		super();
		this.popust = popust;
		this.broj_bodova = broj_bodova;
	}

	public String getPopust() {
		return popust;
	}

	public void setPopust(String popust) {
		this.popust = popust;
	}

	public int getBroj_bodova() {
		return broj_bodova;
	}

	public void setBroj_bodova(int broj_bodova) {
		this.broj_bodova = broj_bodova;
	}

	@Override
	public String toString() {
		return "LoyaltyKartica [popust=" + popust + ", broj_bodova=" + broj_bodova + "]";
	}
	
	

}
