package com.ftn.Knjizara.model;

public enum StatusKomentara {
	
	Na_cekanju,
	Odobren,
	Nije_odobren

}
