package com.ftn.Knjizara.model;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public class Knjige {
	
	private Map<Long, Knjiga> knjige = new HashMap<>();
	private long nextId = 1L;

	public Knjige() {

		try {
			Path path = Paths.get(getClass().getClassLoader().getResource("knjige.txt").toURI());
			System.out.println(path.toFile().getAbsolutePath());
			List<String> lines = Files.readAllLines(path, Charset.forName("UTF-8"));

			for (String line : lines) {
				line = line.trim();
				if (line.equals("") || line.indexOf('#') == 0)
					continue;
				String[] tokens = line.split(";");
				Long id = Long.parseLong(tokens[0]);
				String naziv = tokens[1];
				String ISBN = tokens[2];
				String izdavackaKuca = tokens[3];
				String autor = tokens[4];
				String godina = tokens[5];
				
				String opis = tokens[6];
				//String slika = tokens[7];
				Double cena = Double.parseDouble(tokens[7]);
				String brojstr = tokens[8];
				String tip = tokens[9];
				String jezik = tokens[10];
				String pismo = tokens[11];
				
				


				knjige.put(Long.parseLong(tokens[0]), new Knjiga(id, naziv,ISBN,izdavackaKuca, autor,
						godina, opis,  cena, brojstr, tip,
						pismo, jezik));

				if(nextId<id)
					nextId=id;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Knjiga findOne(Long id) {
		return knjige.get(id);
	}

	public List<Knjiga> findAll() {
		return new ArrayList<Knjiga>(knjige.values());
	}

	public Knjiga save(Knjiga umetnickoDelo) {
		if (umetnickoDelo.getId() == null) {
			umetnickoDelo.setId(++nextId);
		}
		knjige.put(umetnickoDelo.getId(), umetnickoDelo);
		return umetnickoDelo;
	}

	public List<Knjiga> save(List<Knjiga> ud) {
		List<Knjiga> ret = new ArrayList<>();

		for (Knjiga u : ud) {
			Knjiga saved = save(u);

			if (saved != null) {
				ret.add(saved);
			}
		}
		return ret;
	}

	public Knjiga delete(Long id) {
		if (!knjige.containsKey(id)) {
			throw new IllegalArgumentException("tried to remove non existing item");
		}
		Knjiga umetnickoDelo = knjige.get(id);
		if (umetnickoDelo != null) {
			knjige.remove(id);
		}
		return umetnickoDelo;
	}

	public void delete(List<Long> ids) {
		for (Long id : ids) {
			delete(id);
		}
	}

	public List<Knjiga> findByNaziv(String naziv) {
		List<Knjiga> ret = new ArrayList<>();

		for (Knjiga u : knjige.values()) {
			if (naziv.startsWith(u.getNaziv())) {
				ret.add(u);
			}
		}

		return ret;
	}

}
