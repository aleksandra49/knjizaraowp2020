package com.ftn.Knjizara.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;



public class Knjiga {
	
	private Long id;
	private String naziv;
	private String ISBN;
	private String izdavackaKuca;
	private String autor;
	private String godIzdavanja;
	private String opis;
	//private String slika;
	private double cena;
	private String brojstr;
	//private int brojstr;
	private String tip_poveza;
	//private TipPoveza tip_poveza;
	private String pismo;
	//private Pismo pismo; 
	private String jezik;
	//private String ocenaKnjige;
	
	
	public Knjiga() {
	
}



	public Knjiga(String naziv, String iSBN, String izdavackaKuca, String autor, String godIzdavanja, String opis,
			double cena, String brojstr, String tip_poveza, String pismo, String jezik) {
		super();
		this.naziv = naziv;
		ISBN = iSBN;
		this.izdavackaKuca = izdavackaKuca;
		this.autor = autor;
		this.godIzdavanja = godIzdavanja;
		this.opis = opis;
		this.cena = cena;
		this.brojstr = brojstr;
		this.tip_poveza = tip_poveza;
		this.pismo = pismo;
		this.jezik = jezik;
	}



	public Knjiga(Long id, String naziv, String iSBN, String izdavackaKuca, String autor, String godIzdavanja,
			String opis, double cena, String brojstr, String tip_poveza, String pismo, String jezik) {
		super();
		this.id = id;
		this.naziv = naziv;
		ISBN = iSBN;
		this.izdavackaKuca = izdavackaKuca;
		this.autor = autor;
		this.godIzdavanja = godIzdavanja;
		this.opis = opis;
		this.cena = cena;
		this.brojstr = brojstr;
		this.tip_poveza = tip_poveza;
		this.pismo = pismo;
		this.jezik = jezik;
	}




	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Knjiga other = (Knjiga) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public String getISBN() {
		return ISBN;
	}
	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}
	public String getIzdavackaKuca() {
		return izdavackaKuca;
	}
	public void setIzdavackaKuca(String izdavackaKuca) {
		this.izdavackaKuca = izdavackaKuca;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public String getGodIzdavanja() {
		return godIzdavanja;
	}
	public void setGodIzdavanja(String godIzdavanja) {
		this.godIzdavanja = godIzdavanja;
	}
	public String getOpis() {
		return opis;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	public double getCena() {
		return cena;
	}
	public void setCena(double cena) {
		this.cena = cena;
	}
	public String isTip_poveza() {
		return tip_poveza;
	}
	public void setTip_poveza(String tip_poveza) {
		this.tip_poveza = tip_poveza;
	}
	public String isPismo() {
		return pismo;
	}
	public void setPismo(String pismo) {
		this.pismo = pismo;
	}
	public String getJezik() {
		return jezik;
	}
	public void setJezik(String jezik) {
		this.jezik = jezik;
	}


	public String getBrojstr() {
		return brojstr;
	}

	public void setBrojstr(String brojstr) {
		this.brojstr = brojstr;
	}
	
	



	public String getTip_poveza() {
		return tip_poveza;
	}



	public String getPismo() {
		return pismo;
	}

	
	
	
	/*public List<Knjiga> findAll() {
		return new ArrayList<Knjiga>(knjiga.values());
	}*/
	
	


	@Override
	public String toString() {
		return "Knjiga [id=" + id + ", naziv=" + naziv + ", ISBN=" + ISBN + ", izdavackaKuca=" + izdavackaKuca
				+ ", autor=" + autor + ", godIzdavanja=" + godIzdavanja + ", opis=" + opis + ", cena=" + cena
				+ ", brojstr=" + brojstr + ", tip_poveza=" + tip_poveza + ", pismo=" + pismo + ", jezik=" + jezik + "]";
	}



	




	
	
	
	

}
