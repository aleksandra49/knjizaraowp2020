package com.ftn.Knjizara.model;

public class Korisnik {
	
	private String korIme="", lozinka="", email="" ,
			ime="", prezime="", datumRodjenja="" ,
			adresa="", brojTel="", datIvremeReg="" , uloga="";
	private boolean administrator = false;
	private boolean ulogovan = false;
	
	/*private String korIme;
	private String lozinka;
	private String email;
	private String ime;
	private String prezime;
	private String datumRodjenja;
	private String adresa;
	private String brojTel;
	private String datIvremeReg;
	private String uloga;
	//private Uloga uloga;*/
	
	
	public Korisnik() {
		super();
	}
	
	public Korisnik(String korIme, String lozinka, String email, String ime, String prezime, String datumRodjenja,
			String adresa, String brojTel, String datIvremeReg, String uloga) {
		super();
		this.korIme = korIme;
		this.lozinka = lozinka;
		this.email = email;
		this.ime = ime;
		this.prezime = prezime;
		this.datumRodjenja = datumRodjenja;
		this.adresa = adresa;
		this.brojTel = brojTel;
		this.datIvremeReg = datIvremeReg;
		this.uloga = uloga;
	}

	public String getKorIme() {
		return korIme;
	}

	public void setKorIme(String korIme) {
		this.korIme = korIme;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getDatumRodjenja() {
		return datumRodjenja;
	}

	public void setDatumRodjenja(String datumRodjenja) {
		this.datumRodjenja = datumRodjenja;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public String getBrojTel() {
		return brojTel;
	}

	public void setBrojTel(String brojTel) {
		this.brojTel = brojTel;
	}

	public String getDatIvremeReg() {
		return datIvremeReg;
	}

	public void setDatIvremeReg(String datIvremeReg) {
		this.datIvremeReg = datIvremeReg;
	}

	public String getUloga() {
		return uloga;
	}

	public void setUloga(String uloga) {
		this.uloga = uloga;
	}
	
	

	public boolean isAdministrator() {
		return administrator;
	}

	public void setAdministrator(boolean administrator) {
		this.administrator = administrator;
	}

	public boolean isUlogovan() {
		return ulogovan;
	}

	public void setUlogovan(boolean ulogovan) {
		this.ulogovan = ulogovan;
	}

	@Override
	public String toString() {
		return "Korisnik [korIme=" + korIme + ", lozinka=" + lozinka + ", email=" + email + ", ime=" + ime
				+ ", prezime=" + prezime + ", datumRodjenja=" + datumRodjenja + ", adresa=" + adresa + ", brojTel="
				+ brojTel + ", datIvremeReg=" + datIvremeReg + ", uloga=" + uloga + ", administrator=" + administrator
				+ ", ulogovan=" + ulogovan + "]";
	}

	





}
