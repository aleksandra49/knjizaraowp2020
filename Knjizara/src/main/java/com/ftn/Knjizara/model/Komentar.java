package com.ftn.Knjizara.model;

public class Komentar {
	
	private String  tekstKomentara;
	private int ocena;
	private String datumKom;
	private String autoKmen;
	//private Korisnik autorKom;
	private String komentarZaKnj;
	private StatusKomentara status;
	
	
	public Komentar() {
		super();
	}


	public Komentar(String tekstKomentara, int ocena, String datumKom, String autoKmen, String komentarZaKnj,
			StatusKomentara status) {
		super();
		this.tekstKomentara = tekstKomentara;
		this.ocena = ocena;
		this.datumKom = datumKom;
		this.autoKmen = autoKmen;
		this.komentarZaKnj = komentarZaKnj;
		this.status = status;
	}


	public String getTekstKomentara() {
		return tekstKomentara;
	}


	public void setTekstKomentara(String tekstKomentara) {
		this.tekstKomentara = tekstKomentara;
	}


	public int getOcena() {
		return ocena;
	}


	public void setOcena(int ocena) {
		this.ocena = ocena;
	}


	public String getDatumKom() {
		return datumKom;
	}


	public void setDatumKom(String datumKom) {
		this.datumKom = datumKom;
	}


	public String getAutoKmen() {
		return autoKmen;
	}


	public void setAutoKmen(String autoKmen) {
		this.autoKmen = autoKmen;
	}


	public String getKomentarZaKnj() {
		return komentarZaKnj;
	}


	public void setKomentarZaKnj(String komentarZaKnj) {
		this.komentarZaKnj = komentarZaKnj;
	}


	public StatusKomentara getStatus() {
		return status;
	}


	public void setStatus(StatusKomentara status) {
		this.status = status;
	}


	@Override
	public String toString() {
		return "Komentar [tekstKomentara=" + tekstKomentara + ", ocena=" + ocena + ", datumKom=" + datumKom
				+ ", autoKmen=" + autoKmen + ", komentarZaKnj=" + komentarZaKnj + ", status=" + status + "]";
	}
	
	
	

}
