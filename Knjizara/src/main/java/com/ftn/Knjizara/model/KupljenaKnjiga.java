package com.ftn.Knjizara.model;

public class KupljenaKnjiga {
	
	private Knjiga knjiga;
	private String broj_primeraka;
	//private int broj_primeraka;
	private int cena;
	
	
	public KupljenaKnjiga() {
		
	}


	public KupljenaKnjiga(Knjiga knjiga, String broj_primeraka, int cena) {
		super();
		this.knjiga = knjiga;
		this.broj_primeraka = broj_primeraka;
		this.cena = cena;
	}


	public Knjiga getKnjiga() {
		return knjiga;
	}


	public void setKnjiga(Knjiga knjiga) {
		this.knjiga = knjiga;
	}


	public String getBroj_primeraka() {
		return broj_primeraka;
	}


	public void setBroj_primeraka(String broj_primeraka) {
		this.broj_primeraka = broj_primeraka;
	}


	public int getCena() {
		return cena;
	}


	public void setCena(int cena) {
		this.cena = cena;
	}


	@Override
	public String toString() {
		return "KupljenaKnjiga [knjiga=" + knjiga + ", broj_primeraka=" + broj_primeraka + ", cena=" + cena + "]";
	}
	
	
	


}
